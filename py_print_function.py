""""
This can be all options for use the print method/ functions for the print purpose.
"""


# ------------ normal print-------------------
print()
print("Hello Boss")
print('Hello Boss with single code')
print()


# ------------ print value with comas ------------
print("Ram", "Shyam", "Hari")
print()


# ------------ print value using end= "\t" ------------
"""
we can give end = anything which is able to seperate the sentences, like " ", "\t", "\v", etc. 
"""
print("Hello, I'm", end="\t")
print("Nawaraj Jaishi")

print("hello \'Nawaraj jaishi\' ")
print("hello \
    \'Nawaraj jaishi\'\
    how are you man? ")
print()


# ------------ print value of list ------------
l = ['name', "age", "class"]
t = ("hey", "man", "how", "are", "you", "?")
d = {"name": "nawaraj", "age": 24}

print("list: ", l)
print("Touple: ", t)
print("Dictionary: ", d)


# ----------- input type conversition -------------
print()
name = input("Name: ")
phone = int(input("Your Ph. Number: "))
print()

print(f"Hi \"{name} \" welcome in programming",
      f"Your Contact Number is \"{phone}\" ", sep="\n")
