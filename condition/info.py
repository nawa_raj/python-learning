# inline if condition as ternary operator in javascript


"""
the following expression assing a's value if a has value otherwise it assign 45 to it.
"""

print()
print("----------- inline if else condition ------------------ ")
print()

a = 0
x = a if a else 45

# it execute else part i.e. it assign 45 into x
print(x)
print()


a = 100
x = a if a else 45
# it execute true and assign 100 into x.
print(x)
print()


# single if statement
a = True
if a:
    print("it worked")
print()
