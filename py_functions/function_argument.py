#  this file shows many ways to pass arguments to the function

""" 
positional arguments
    we can send limited arguments

"""
print("\n------------- positional arguments ------------------\n")


# subtraction
def subtraction(a, b):
    return a - b


print("function work as (a - b)")
print("sub of subtraction(10, 3): ", subtraction(10, 3))
print("sub of subtraction(3, 10): ", subtraction(3, 10))

# following statemet give error.
# print("sub of subtraction(3, 10, 23): ", subtraction(3, 10, 23))
# print("sub of subtraction(3): ", subtraction(3))
print()


""" 
keyword arguments
    we can receive limited arguments which are defined as 'formal arguments'

"""
print("\n------------- keyword arguments ------------------\n")


# subtraction
def subtraction(a, b):
    return a - b


print("function work as (a - b)")
print("sub of subtraction(a= 10, b= 3): ", subtraction(a=10, b=3))
print("sub of subtraction(a= 3, b= 10): ", subtraction(a=3, b=10))
print("sub of subtraction(b= 3, a= 10): ", subtraction(b=3, a=10))
print()


""" 
default arguments
    we can mention default value to the parameter of function
    when we define default value of parameter we need to declare that parameter at last 
    otherwise we need to provide default value to all rest parameters

"""
print("\n------------- default arguments/parameter ------------------\n")


def addition(num2, num1=28):
    return num1 + num2


print("addition(23): ", addition(23))
print("addition(23, 78): ", addition(23, 78))
print("addition(num2 = 89): ", addition(num2=89))
print()


""" 
variable length arguments
    we can receive too many arguments from the function call as actual arguments
    as the tuple datatype format 

"""
print("\n------------- variable length arguments ------------------\n")


# subtraction
def subtraction(*num):

    print()
    print("received arguments: ", num)

    return num[0] - num[1]


print("function work as (num[0] - num[1])")
print()

print("subtraction(10, 20): ", subtraction(10, 20))
print("subtraction(20, 7.5): ", subtraction(20, 7.5))
print(
    "subtraction(20, 7.5, 12, 54, 67, 76 ,6865,88): ",
    subtraction(20, 7.5, 12, 54, 67, 76, 6865, 88),
)

# if we call function like as follow it throw error
# print("sub of subtraction(a= 10, b= 3): ", subtraction(a=10, b=3))
# print("sub of subtraction(a= 3, b= 10): ", subtraction(a=3, b=10))
# print("sub of subtraction(b= 3, a= 10): ", subtraction(b=3, a=10))
print()


# another subtraction version
def subtraction(x, *num):

    print()
    print("positional 'x' argument value: ", x)
    print("received arguments: ", num)

    return x - num[0] - num[1]


print()
print("function work as (x - num[0] - num[1])")
print("subtraction(20, 12, 2)", subtraction(20, 12, 2))
print("subtraction(-45, 50, 60)", subtraction(-45, 50, 60))
print()


# addition
def addition(*num):
    sum = 0

    for x in num:
        sum += x
    return sum


print()
print("add all provided parameter value and return sum")
print(
    "addition(21, 432, 1, 23, 2345, 456, 24.435, -845, 758947, 80.8719) result: ",
    addition(21, 432, 1, 23, 2345, 456, 24.435, -845, 758947, 80.8719),
)
print()


""" 
keyword variable length arguments
    we can receive too many arguments from the function call as actual arguments
    as dictionary datatype format i.e. key value format

"""
print("\n------------- keyword variable length arguments ------------------\n")


def objectParameter(**kwargs):
    print("parameter type: ", type(kwargs))
    return kwargs


# pass dictionary as argumants
print(objectParameter(**{"name": "nawaraj jaishi", "age": 28, "class": "BIM"}))
print()


# pass key value as arguments
print(
    objectParameter(
        a=20,
        b=30,
        name="ram sharan sharma",
        age=20,
        gpa=23.34,
        address={"district": "bajura", "village": "kolti", "wardno": 2},
    )
)
print()


print()
print("positional arguments with keyword variable length arguments")
print()


def objectParameter(x, **kwargs):
    print("parameter type: ", type(kwargs))
    print("extra positional argument 'x': ", x)
    return kwargs


print(
    objectParameter(
        30,
        a=20,
        b=30,
        name="ram sharan sharma",
        age=20,
        gpa=23.34,
        phone={"mobile": 98352612538, "landline": str("06318908")},
    )
)
print()
