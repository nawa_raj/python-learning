#  nested function can be used to split code into several parts


def parent(name):
    def child(name):
        print("I'm Child function")
        print("your parameter: ", name)
        print()

    print("I'm parent function")
    print("your parameter: ", name)
    print()

    child(name)


# calling function:
print()
parent("nawaraj jaishi")
print()


# return inner function from outer function / function return another function


def parent1():
    # this counter has local scope i.e. even not accessible to its child class.
    counter = 0

    def child():
        # nonlocal keyword make counter is not a local variable. its may be parent or global variable
        # if we not use nonlocal keyword counter is not accessible here.
        nonlocal counter
        counter += 1

        return counter

    return child()


print()
# parent return value as integer value
print(
    "------------ returning child as child() from parent function --------------------"
)
print()
print(parent1())
print(parent1())
print(parent1())
print()


# slight different
# parent return child as callback function
print(
    "\n----------- returning child function as child from parent function ---------------\n"
)


def parent2():
    counter = 0

    print("parent function 'counter' variable initial value: ", counter)

    def child():
        # if we trying to override or change value of parent function variable we cannot do simply
        # we need to make it nonlocal

        nonlocal counter
        counter += 1

        return counter

    print("it change value of parent function variable 'counter': ", counter)

    return child


fun = parent2()

print(fun())
print(fun())
print(fun())
print()


# another waay to call innter function
print()


def dis(sh):
    print("Display function")

    return sh


def show():
    return "show function"


r_sh = dis(show)
print(r_sh())
print()


# change outer function variable value
# parent return child as callback function
print(
    "\n----------- change outer function variable value from inner function ---------------\n"
)


def parentfn():
    counter = 0

    print("initial value of 'counter' variable in parent function: ", counter)

    def child():
        # if we trying to override or change value of parent function variable we cannot do simply
        # we need to make it nonlocal
        nonlocal counter
        counter += 3

        print("\n'counter' value inside inner function: ", counter)

    child()
    print("\n'counter' variable value outside inner function: ", counter)


parentfn()
print()
