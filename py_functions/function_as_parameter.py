#  we can pass function as parameter in python...


def display(message):

    print(message)


def welcome(name):
    return f"Welcome Mr/Ms {name}"


# call display function arguments as welcome function name:
print()
display(welcome("Nawaraj jaishi"))
print()
