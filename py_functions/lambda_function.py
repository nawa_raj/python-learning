#  we can use lambda function as one line arrow function in javascript


# add
print("\n------ add two number with lambda function -------------\n")

sum = lambda x, y: x + y

print(sum(56, 79))
print()


# add and sub
print("\n------ return multiple value form lambda function -------------\n")

add_sub = lambda x, y: (x + y, x - y)


print(add_sub(56, 79))
print(*add_sub(56, 79))

a, s = add_sub(45, 67)

print(a, s)
print()


# nested lambda function
print("\n---------------- nested lambda function-----------\n")

add = lambda x=10: (lambda y: x + y)

a = add()

print(a(20))
print()


# pass lambda function to another function
print("\n---------------- pass lambda function to another function -----------\n")


def show(a):
    print(a(8))


show(lambda x: x)
print()


# return lambda function from a function
print("\n---------------- pass lambda function to another function -----------\n")


def add():
    y = 20

    return lambda x: x + y


a = add()
print(a(90))
print()
