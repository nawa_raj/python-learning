"""
IIFE function stand for Immediately Invoked Function Expression

this function is called immediately weh we write it 
this type of function is not recommendet for to use it at now.

"""


# (lambda x, y: x + y)(23, 56)
print("\n-------------- IIFE Function ------------------\n")
print("(lambda x, y: x + y)(23, 56): \n")

(lambda x, y: print(x + y))(23, 56)

print()
