"""
function creation syntax: 
    def add(x, y):
        sum = x + y 
        return sum 

above function defination: 
    def ->      represent starting of function defination 
    add ->      function name 
    () ->       parenthesis represent it is a function not a variable.
    (x, y) ->   parameter of the function which is required 
    : ->        describe, begining of function body

when we call function the syntax be like:
    add(20, 30)

    (20, 30) -> 20 and 30 are arguments 
"""


# simple example
def gretting_message(name):
    print()
    print(f"Welcome Mr/Mis '{name.title()}' in python learning session")
    print()


gretting_message("nawaraj jaishi")
gretting_message("rani mukhargi")


def add(x: int, y: int):
    sum = x + y
    return sum


def add_sub(x: int, y: int):
    sum = x + y
    sub = x - y
    return sum, sub


print()
print("sum of add(20, 30): ", add(20, 30))
print("sum of add(-20, -30): ", add(-20, -30))
print("sum of add(-20, 30): ", add(-20, 30))
print()


print()
sum, sub = add_sub(20, 30)
print("sum, sub = add_sub(20, 30): ", sum, sub)
print("add_sub(-20, -30): ", add_sub(-20, -30))
print("add_sub(-20, 30): ", add_sub(-20, 30))
print()
