# recursive function is the function which call itself as loop thorough the operations...


# nested function with recursive function call
def factorial(num):
    if not isinstance(num, int):
        raise TypeError("Sorry, Number must be a integer.")

    if num < 0:
        raise ValueError("Sorry, Number must be zero or positive")

    # calculate the factorial of number
    def calFactorial(num):
        if num <= 1:
            return 1
        return num * calFactorial(num - 1)

    return calFactorial(num)


def new_factorial(num):
    if not isinstance(num, int):
        raise TypeError("Sorry, Number must be a integer.")

    elif num < 0:
        raise ValueError("Sorry, Number must be zero or positive")

    else:
        if num <= 1:
            return 1

        return num * new_factorial(num - 1)


print("\n------------ calculate factorial of 5 -------------------\n")

print("nested function with recursive function: ", factorial(5))
print()

print("simple recursive function: ", new_factorial(5))
# print(str(new_factorial))
