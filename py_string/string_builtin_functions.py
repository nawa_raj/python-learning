"""
python has so many interactive built in function which helps to work with string value 
in python and format, manage strings.

"""


# upper case
print()
print("------- upper() ---------------")
print()

name = "Nawaraj jaishi"

print("Orginal String: ", name)
print("after conversion: ", name.upper())
print()


# lower case
print()
print("------- lower() ---------------")
print()

name = name.upper()

print("orginal string: ", name)
print("after conversion: ", name.lower())
print()


# capitalize case
print()
print("------- capitalize() ---------------")
print()

name = name.lower()

print("orginal string: ", name)
print("after conversion: ", name.capitalize())
print()


# tital case
print()
print("------- tiitle() ---------------")
print()

print("orginal string: ", name)
print("after conversion: ", name.title())
print()


# swapcase case : it convert all string element into upper if it is lover and vice versa.
print()
print("------- swapcase() ---------------")
print()


print("orginal string: ", name)
print("swapcase lower to upper: ", name.swapcase())

name = name.upper()
print()
print("orginal string: ", name)
print("swapcase upper to lower: ", name.swapcase())
print()


# casefold: it convert all string lower case with aggrssion.
"""
Casefolding is similar to lowercasing but more aggressive because it is intended to 
remove all case distinctions in a string.

For example, the German lowercase letter 'ß' is equivalent to "ss". 
Since it is already lowercase, lower() would do nothing to 'ß'; casefold() converts it to "ss".
"""


print()
print("------- casefold() ---------------")
print()

newname = "Nawaraj jaishi ß"

print("orginal string: ", newname)
print("casefold convert string to lower case with aggression: ", newname.casefold())
print()


# isupper()
print()
print("------- isupper() ---------------")
print()

name = name.title()

print("orginal string: ", name)
print("is upper: ", name.isupper())

name = name.upper()
print()
print("orginal string: ", name)
print("is upper: ", name.isupper())
print()


# islower()
print()
print("------- islower() ---------------")
print()

name = name.title()

print("orginal string: ", name)
print("is lower: ", name.islower())

name = name.lower()
print()
print("orginal string: ", name)
print("is lower: ", name.islower())
print()


# istitle()
print()
print("------- istitle() ---------------")
print()

name = name.title()

print("orginal string: ", name)
print("is title: ", name.istitle())

name = name.lower()
print()
print("orginal string: ", name)
print("is title: ", name.istitle())
print()


# isdigit()
print()
print("------- isdigit() ---------------")
print()

name = name.title()

print("orginal string: ", name)
print("is there only digit: ", name.isdigit())


semiDigit = "hello 123456"
print()
print("orginal string: ", semiDigit)
print("is there only digit: ", semiDigit.isdigit())

di = "1325125131"
print()
print("orginal string: ", di)
print("is there only digit: ", di.isdigit())
print()


# isalpha()
"""
it return True if string have at lease one character[A to Z or a to z] only.

even if there space it return False

"""

print()
print("------- isalpha() ---------------")
print()

newName = "Nawaraj"

print("orginal string: ", newName)
print("is alpha string ?: ", newName.isalpha())


alph = "hello !!!, how are you?"
print()
print("orginal string: ", alph)
print("is alpha string ?: ", alph.isalpha())

di = "1325125131"
print()
print("orginal string: ", di)
print("is alpha string ?: ", di.isalpha())
print()


# isalnum()
"""
it return False even thre is one single space.
"""
print()
print("------- isalnum() ---------------")
print()


print("orginal string: ", name)
print("is string  alpha numeric ?: ", name.isalnum())


alph = "hey1234"
print()
print("orginal string: ", alph)
print("is string  alpha numeric ?: ", alph.isalnum())

di = "1325125131"
print()
print("orginal string: ", di)
print("is string  alpha numeric ?: ", di.isalnum())
print()


# isspace()
"""
it return True if there is only space value not other in string.
"""
print()
print("------- isspace() ---------------")
print()


print("orginal string: ", name)
print("is string has only space ?: ", name.isspace())


spa = " "
print()
print("orginal string: ", spa)
print("is string has only space ?: ", spa.isspace())


# center()
"""
syntax: str.center(stringWidth, fillValue)

"""

print()
print("------- center() ---------------")
print()

name = name.title()

print("orginal string: ", name)
print("after conversion: ", name.center(20, "-"))
print()


# count()
"""
syntax: str.count(value, startIndex, endIndex)

"""

print()
print("------- count() ---------------")
print()

str1 = "hello, how are you ?, how you feeling now ?, how about you bro ?"

print("orginal string: ", str1)
print("'how' appear: ", str1.count("how"), "times")
print()


# lstrip()
"""
syntax: str.lstrip()

it remove space from left side of string

"""

print()
print("------- lstrip() ---------------")
print()

str1 = "     hello, how are you ?"

print("orginal string: ", "whitespace",  str1)
print("lstrip remove space from left: ", "whitespace", str1.lstrip())
print()


# rstrip()
"""
syntax: str.rstrip()

it remove space from right side of string

"""

print()
print("------- rstrip() ---------------")
print()

str1 = "hello, how are you ?                    "

print("orginal string: ", str1, "whitespace")
print("rstrip remove space from right: ", str1.rstrip(), "whitespace")
print()


# strip()
"""
syntax: str.strip()

it remove space from both side of string

"""

print()
print("------- strip() ---------------")
print()

str1 = "     hello, how are you ?                  "

print("orginal string: ", "whitespace",  str1, "whitespace")
print("strip remove space from left: ", str1.strip())
print()


# replace()
"""
syntax: str.replace(oldSubString, newSubString, count)

it replace old sub string with new one

"""

print()
print("------- replace() ---------------")
print()

name = name.lower()
print("orginal string: ", name)
print("new Name: ", name.replace("nawaraj", "Raj"))


print()
newname = "nawaraj nawaraj jaishi jaishi nawaraj"
print("orginal string: ", newname)
print("new Name: ", newname.replace("nawaraj", "Raj"))

print()
newname = "nawaraj jaishi nawaraj jaishi nawaraj"
print("orginal string: ", newname)

# it replace first 2 occurance of substring
print("newname.replace('nawaraj', 'Raj', 2): ",
      newname.replace("nawaraj", "Raj", 2))
print()


# split()
"""
syntax: str.split(seperator)

it split string by provided seperator

"""

print()
print("------- split() ---------------")
print()

str1 = "hello, how are you ?"
str2 = "hello, how, are, you, ?"

print("orginal string: ", str1)
print("orginal string: ", str2)
print("split by str1.split(' '): ", str1.split(" "))
print("split by str2.split(','): ", str2.split(","))
print("split by str1.split(' ', 2): ", str1.split(" ", 2))
print()


# join()
"""
syntax: "seperator".join(list/tuple/string_list)

it joins list of string into single string

"""

print()
print("------- join() ---------------")
print()

list1 = ["how", "are", "you", "?"]
tuple1 = ("ram", "shyam", "hari", "gita")

print("orginal list: ", list1)
print("orginal tuple: ", tuple1)
print("join by ' '.join(list1): ", " ".join(list1))
print("join by ','.join(tuple1): ", ",".join(tuple1))
print()


# startswith()
"""
syntax: str.startswith(subString)

it return True if string start with provided substring.

"""

print()
print("------- startswith() ---------------")
print()

name = name.lower()
print("orginal string: ", name)
print("name.startswith('nawaraj'): ", name.startswith("nawaraj"))
print("name.startswith('jaishi'): ", name.startswith("jaishi"))
print()


# endswith()
"""
syntax: str.endswith(subString)

it return True if string ends with provided substring.

"""

print()
print("------- endswith() ---------------")
print()

name = name.lower()
print("orginal string: ", name)
print("name.endswith('jaishi'): ", name.endswith("jaishi"))
print("name.endswith('nawaraj'): ", name.endswith("nawaraj"))
print()


# encode()
"""
syntax: str.encode(encoding)

"""

print()
print("------- encode() ---------------")
print()

str1 = "hello, how are you ?, how you feeling now ?, how about you bro ?"

print("orginal string: ", str1)
print("encoded into Arabic: ", str1.encode("cp720"))
print()
