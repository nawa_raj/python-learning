
from datetime import datetime


"""
A formatted string literal or f-string is a string literal that is prefixed with f or F.

This strings may contain replacement fields, which are expression delimited by curly braces{}.
while other strring literals always have a constant value, formatted string are really 
expressions evaluated at run time.

syntax: 
    f"{index/key/name:[fill][align][sign][#][0][width][,][.precision]type}"

format specification:
    [fill][align][sign][#][0][width][,][.precision]type

"""


# simple concept

print()
print("------- f string formatter literal -------------")
print()

age = 24
name = 'namearaj jaishi'


# accessing simple variables
print(f"age: {age} name: {name}")
print()


# --------- integer value -----------------
print()
print("--------- integer value -------------")
print()

num = 1234

# define type
print(f"{num:d}")

# define width and type
print(f"{num:10d}")

# define fill value
print(f"{num:010d}")

# define sign
print(f"{num:+10d}")

# left align
print(f"{num:<10d}")

# left align with fill value
print(f"{num:*<10d}")

# right align with fill value
print(f"{num:$>10d}")

# center align
print(f"{num:^10d}")

# center align with fill value
print(f"{num:-^10d}")
print()

# thausand seperator
print(f"{num:,}")
print()


# --------- float value -----------------
print()
print("--------- integer value -------------")
print()

flnum = 23.9345

# define type
print(f"{flnum:f}")

# define type with width
print(f"{flnum:10f}")

# define fill zero value
print(f"{flnum:010f}")

# define length of decimal part
print(f"{flnum:.20f}")

# define width and decimal length zero value
print(f"{flnum:10.2f}")

# define sign value
print(f"{flnum:+10.2f}")

# left align
print(f"{flnum:<10.2f}")

# left align with fill value
print(f"{flnum:-<10.2f}")

# right align with fill value
print(f"{flnum:*>10.2f}")

# center align with fill value
print(f"{flnum:_^10.2f}")


# --------- float value -----------------
print()
print("--------- string value -------------")
print()


str1 = "nawaraj jaishi"

print(f"{str1:s}")
print(f"{str1:20s}")
print(f"{str1:020s}")
print(f"{str1:<20s}")
print(f"{str1:-<20s}")
print(f"{str1:->20s}")
print(f"{str1:*^20s}")

print(f"{str1:.5s}")
print(f"{str1:20.5s}")
print(f"{str1:<20.5s}")
print(f"{str1:-<20.5s}")
print(f"{str1:->20.5s}")
print()


# thousand seperator
num2 = 1423148721946382

print(f"{num2:,}")
print(f"{num2:_}")
print()


# something
print(f"{10*800}")
print(f"{20/3:.2}")
print(f"{20/3:.2%}")
print()


# accessing tuple value
print("---- accessing tuple value ------")
print()

tup = ("Ramsharan", 30)
print(f"{tup[0]} {tup[1]}")
print()


# accessing dict variables
print("---- accessing dict value ------")
print()

d = {'name': 'nawaraj jaishi', 'grade': 3.789}

print(f"name: {d['name']} \t grade: {d['grade']}")
print()


# to print bracket with f string literals
print(f"{{10}}")


# date format with f string literals
print()
print("-------- datetime format with f string literal ---------------")
print()

today = datetime.today()

print(f"{today:%Y-%b-%d}")
print()
