"""
here we learn ho wto define and work with strings in python.
"""


str1 = "hello"
str2 = 'hello! nawaraj jaishi'

# multiline decleration
str3 = '''
        hello!!! nawaraj jaishi
        how are you ?
        are you learning python now ?
    '''

str4 = """
        hey man! 
        how are you ?
        are you there ?
        """


print()
print("---------------- string decleration in python --------------")
print()

print('defined with double courts("...") of str1: ', str1)
print()
print("defined with single court('...') of ststr2: ", str2)
print()
print("defined with triple single courts('''...''') for multiline string of str3: ", str3)
print()
print('defined with triple double courts("""...""") for multiline string of str4: ', str4)
print()


# raw text print string as like its defined or assigned...
str5 = r"Hello \tHow are you ?"
print("row text(r'...'): ", str5)
print()


# find the index of string values...
print()
print("character at 2 index of str1[2]: ", str1[2])
print("character at 12 index of str4[12]: ", str4[12])

# accessing each element with while loop
print()
print("------------ accessing each element of str2 --------")
i = 0

while i < len(str2):
    print("index ", i, " = ", str2[i])
    i += 1

print()

print()
print("------------ accessing elements using slicing into str3 --------")
print("str3[:20]: ", str3[:20])
print("str3[20:40]: ", str3[20:40])
print("str3[20::5]: ", str3[20::5])
print("str3[:-20]: ", str3[:-20])

print()
print("str3[-20:]: ", str3[-20:])
print("str3[-20:-30]: ", str3[-20:-10])
print()


# repetation operators
print()
print("--------- Repetation Operator ----------------")
print("repetation operators('$'*7): ", "$"*7)
print()


# repetation operators
print()
print("--------- Concatenation Operator ---------------")
str12 = "nawaraj jaishi".title()

print("str12: ", str12)
print("Concatenation operators 'Hello + str12': ", "Hello" + " " + str12)
print()
