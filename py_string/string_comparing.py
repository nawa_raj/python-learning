
"""
here we compare string with the help of comparision operators 
"""

str1 = "nawaraj jaishi"
str2 = str1

str3 = "hello nawaraj jaishi"
str4 = "where are you ?"

print()
print("--------- String Comparisation ---------------")
print()


print("str1: ", str1)
print("str2: ", str2)
print("str3: ", str3)
print("str4: ", str4)
print()


# check is string value equal
print("str1 == str2: ", str1 == str2)
print("str1 != str2: ", str1 != str2)
print()


# compare letter occurance in aplhabets
"""
it means in string a alphabet is greater from another alphabet if first alphabet is occure after in
all alphabets...

e.g.  a is less than b, because b comes after a. so letter b's value is higher than 'a'
"""


print("str1 >= str4: ", str1 >= str4)
print("str1 < str3 :", str1 < str3)

print()
