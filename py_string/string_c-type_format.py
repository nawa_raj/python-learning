"""
there are three formatting option in python for string formatting.

 1. C style String Formatting
 2. format() method 
 3. f-string / Formatted String Literals


 1. C-style String Formatting:
    syntax: 
        print("format placeholder", %(data))

    format placeholder:
        %[flags][width][.percision]type 
    

"""


# C - style String Formatting:

# this modulo technique required positional argunments.
# place integer value in string
print()
print("place integer with '%d': ", "hey man you got %d marks" % 432)
print()


# place float value in string
print()
print("place float with '%f': ", "hey man you got %f marks" % 4.32)
print()


print()
print("place double float with '%f %f': ",
      "hey man you got %f marks in math and %f in science" % (4.32, 23.9))
print()


# place string value in string
print()
print("place string with '%s': ", "Hello, %s" % "Nawaraj Jaishi")
print()


print()
print("place multiple string with '%s %s': ",
      "Hello, %s %s" % ("Mr.", "Nawaraj Jaishi"))
print()


"""
Now we use dictionary technique i.e. key value pair format to dynamically put value in string.. 
"""

print()
print("-------- by using key value pair technique --------------")
print()


print("Name: %(name)s \nAge: %(age)d \nGPA: %(gpa)f" %
      {"name": "Nawaraj jaishi", "age": 24, "gpa": 3.5})
print()


# using flag with format
print()
print("----------- using flag in string formatting -------")
print()

print("%d" % 432)    # without flag
print("% d" % 432)   # with space flag
print("%+d" % 432)   # with + flag
print()


# using percision in string formatting...
print()
print("----- using percision in string formatting ----------")
print()


print("%f" % 432.123)    # without percision

# with percision. it remove python added zeros after .123
print("%.3f" % 432.123)

# it create 8 boxes for this sting and place 432 from last but it leave empty box after value is write
print("%8d" % 432)

# it add zeros before 432 till 8 boxes not filled
print("%08d" % 432)

# it create total 8 boxes fill value from right but it only hold 2 digit after decimal
print("%9.2f" % 432.123)
