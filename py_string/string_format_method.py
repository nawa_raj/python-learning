"""
string format method is recently used method to format string and add dynamic values into string

syntax:
    str.format(args)

format placeholder:
    :[fill][align][sign][#][0][width][,][.precision]type

"""


print()
print("-------- string format mehod -------------------")
print()


print("{} your age is {} years ?".format("Hi, nawaraj jaishi", 24))
print("Num1 = {num1}, Num2 = {num2}".format(num1=20, num2=30))

# when we defined as key value don't need to index value at exact index
print("Num1 = {num1}, Num2 = {num2}".format(num2=30, num1=20))

print("{}".format(12.45))
print()


# we can access value using index also
print("{0} {1}".format(21, "hey man"))
print()


# formatting format method using format placeholder
print()
print("---------- format() with placeholer ------------")
print()


# type define
print("{num:d}".format(num=123))


# width define i.e. it create 5 boxex for the string
print("{num:10d}".format(num=123))


# expty box fill with zero value
print("{num:010d}".format(num=123))


# align value (left align)
print("{num:<10d}".format(num=123))
# right align
print("{num:>10d}".format(num=123))


# left align with fill value
print("{num:@<10d}".format(num=123))


# center the value
print("{num:^10d}".format(num=123))


# center with fill value
print("{num:*^10d}".format(num=123))


# sign add
print("{num:+10d}".format(num=123))
print()


"""
work with float number 
"""


# work with flot number
print()
print("------ work with float number --------")
print()


# normal
print("{}".format(123.2453))

# type defined
print("{:f}".format(123.2453))


# type and index defined
print("{0:f}".format(123.2453))


# width defined
print("{:10f}".format(123.2453))
print()


# limit point value
print("{:10.3f}".format(123.2453))


# add plus sign
print("{:+10.3f}".format(123.2453))


# left align
print("{:<10.3f}".format(123.2453))


# left align with fill value
print("{:*<10.3f}".format(123.2453))


# right align with fill value
print("{:*>10.3f}".format(123.2453))


# center align with fill value
print("{:*^10.3f}".format(123.2453))
print()


"""
precision use 
"""

print()
print("---------- use of precision -------------")
print()


# acccessing limiting words
print("{str:.3}".format(str="nawaraj jaishi"))


# accessing 5 letter from 15 total width
print("{str:15.5}".format(str="Nawaraj Jaishi"))


# accessing 5 letter with 15 width and left align and fill value
print("{str:*<15.5}".format(str="Nawaraj Jaishi"))


# accessing 5 letter with 15 width and right align and fill value
print("{str:0>15.5}".format(str="Nawaraj Jaishi"))


# accessing 5 letter with 15 width and center align and fill value
print("{str:*^15.5}".format(str="Nawaraj Jaishi"))
print()


"""
comma option
"""

print()
print("---------- use of comma -------------")
print()


# gitve comma to per thousend value
print()
print("{:,}".format(132142183916874612))

# give _ to every thousand value
print("{:_}".format(63127362198721))


"""
format string from dict data and tuple data 
"""

print()
print("---------- format touple and dict data type -------------")
print()


# assigning tuple value...
tup = ("nawaraj jaishi", 24)
tup2 = ("Raja Ram Chaudari", 20)

print("Name: {0[0]}, Age: {0[1]}".format(tup))

# using spread parameter
print("Name: {}, Age: {}".format(*tup))

print()
print("Name: {0[0]},{1[0]}\nAge: {0[1]}, {1[1]}".format(tup, tup2))


# assigning dictionary
print()
d = {"name": "nawaraj jaishi", "age": 24, "grade": 3.67}

print("name: {0[name]:s}, age: {0[age]:d} grade: {0[grade]:f}".format(d))

# format parameter
print("name: {name:s}, age: {age:d} grade: {grade:f}".format(**d))
print()
