#  with the help of loop we try to access dictionary valye

dict1 = {'name': "nawaraj jaishi", 'class': 12, 'rollno': 45, 'gpa': 3.56}


# getting dict key
print()
print("getting key of the dict with for loop")
print()

for key in dict1:
    print(f"key: {key}")

print()


# getting items of dict with dict.items()
print()
print("using .items() with dict")
print()

print(dict1.items())

print()


# getting key and value at the same time of dict with dict.items() and for loop
print()
print("getting key and value of dict at the time of loop iterate over")
print()

for key, value in dict1.items():
    print(f"key: {key}, value: {value}")

print()
