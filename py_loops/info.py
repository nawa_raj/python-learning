"""
this file holds all types of loops which are used in python to perform certain actions or task
"""


# ---------------- while loops --------------------------
print()
print("-------------- While Loop ------------------------")

# single while loop
count = 1
while count < 10:
    print(f"while loop executed {count} times")
    count += 1


# while loop with else clause
print()
print("---while loop with else clause---")
count = 1
while count < 10:
    print(f"while loop executed {count} times")
    count += 1
else:
    print()
    print("This \"while loop else clause\" is executed only if while loop condition is false \
        \n this means this \"else clause\" is executed at once")


# technique with while loop in python
print()
print("using iterator function with infinite while loop: ")

fruits = ["apple", "orange", "kiwi"]

# Creating an iterator object
# from that iterable i.e fruits
iter_obj = iter(fruits)

# Infinite while loop
while True:
    try:
        # getting the next item
        fruit = next(iter_obj)
        print(fruit)

    except StopIteration:
        # if StopIteration is raised,
        # break from loop
        break

# break statement with while loop
print()
print("--- break statement in while loop ---")
count = 1
while count <= 10:
    print(count)

    if(count == 8):
        break

    count += 1


# continue statement with while loop
print()
print("--- continue statement in while loop ---")

count = 1

while count <= 10:

    if(count % 2 != 0):
        count += 1
        continue

    print(count)
    count += 1


# nested while loop
print()
print("--- nested while loop ---")

count = 1

while count <= 3:
    print()
    count += 1

    j = 1
    while j < count:
        print(j, end=" ")
        j += 1
print()

# ------------- for loop ------------------------------
print()
print("-------------- for Loop ------------------------")

st = "nawaraj jaishi"
print("string value st = ", st)

print()
print("extract each character from 'str' using for loop: ")
for ch in st:
    print(ch)


# for loop with range function
print()
print("for loop with range: for x in range(10): ")

for x in range(10):
    print(x)


# for loop with else clause
print()
print("for loop with else clause: ")

for x in st:
    print(x)
else:
    print("this else part run once at a time even if loop is not executed")


# for loop break statement
print()
print("for loop with break statement: ")

for x in st:
    if(x == "j"):
        print(f"if it find 'j' in {st} after that it simply break the loop")
        break

    print(x)


# for loop continue statement
print()
print("for loop with continue statement: ")

for x in st:
    if(x == "j"):
        print(
            f"if it find 'j' in {st} after that it simply skip to print 'j' letter")
        continue

    print(x)


# for loop pass statement
print()
print("for loop with pass statement: ")

for x in st:
    if(x == "j"):
        pass
        print("this simply pass nothing doing...")

    print(x)


# nested for loop
print()
print("--- nested for loop ---")

for x in range(10, 0, -1):
    for j in range(x, 0, -1):
        print(j, end=" ")

    print()


# ------------- range function ------------------------------
#  range(start, stop, step)
#  formula range(i, j, k) => i, i+k, i+2k
print()
print("range in normal form 'list(range(10))': ", list(range(10)))
print("range with start and stop value 'list(range(1, 8))': ", list(range(1, 8)))
print("range with start, stop and step value 'list(range(2, 8, 2))': ",
      list(range(2, 8, 2)))

print()
print("--- get reverse serial number from range ---")
print("tuple(range(10, 0, -1)): ", tuple(range(10, 0, -1)))
print("tuple(range(10, 0, -2)): ", tuple(range(10, 0, -2)))
print()
