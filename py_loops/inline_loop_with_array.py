# generate a new array from the array which are even number

arr = [*range(100)]

even = [v for v in arr if v % 2 == 0 and v != 0]

print()
print("generate array of even number from array of natural number of 100")
print()

print(even)


# array of number which start with 2 value
startwith2 = [x for x in arr if str(
    x).startswith('2') or str(x).endswith('2')]


print()
print("array of number which have 2 with number")
print(startwith2)
print()
