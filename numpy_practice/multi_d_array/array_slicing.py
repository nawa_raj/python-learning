from numpy import array


"""
syntax:
    array[start:stop:stepsize, start:stop:stepsize]

    first part represent row and second part column onfiguration

    array[0, :] = all element of array's first row

"""


print()
print("-------- 2D array slicing ---------------")
print()


a = array([
    [10, 11, 12, 13],
    [20, 21, 22, 23],
    [30, 31, 32, 33],
    [40, 41, 42, 43]
], dtype=int)

print("orginal array: ", a)
print()

print("0th row with all elements 'a[0, :]': \t\t", a[0, :])
print("0th row with elements after 2 index 'a[0, 2:]':\t\t", a[0, 2:])
print(
    "0th row and all elements with index difference of 2 'a[0, ::2]': \t\t", a[0, ::2])

print("new array from array 'a' with 'a[0:2, 1::2]': \t\t", a[0:2, 1::2])
print("new array from array 'a' with 'a[0:2, :]': \t\t", a[0:2, :])
print("new array from array 'a' with 'a[0:4:2, ::2]': \t\t", a[:3:2, :3:2])
