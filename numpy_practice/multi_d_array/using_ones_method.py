from numpy import *


"""
we can create a ones's n dimentional array in python with the help of numpy

syntax:
    numpy.ones((rows, columns))

3D syntax:
    numpy.ones((no.of array, rows, column))


"""


# create ones array with 5 rows and 8 columns
a = ones((5, 8), dtype=int)

print()
print("------- ND arrays with ones functions ----------")

print("2D array with ones function 'ones((5, 8))':\n", a)
print()

b = ones((2, 3, 4), dtype=int)
print("array b: ", b)
print("Type of array: ", type(b))
