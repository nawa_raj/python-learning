from numpy import *


"""
we can create a zeros n dimentional array in python with the help of numpy

syntax:
    numpy.zeros((2, 3))
"""


# create zeros array with 5 rows and 8 columns
a = zeros((5, 8), dtype=int)

print()
print("------- ND arrays with zeros functions ----------")

print("2D array with zeros function 'zeros((5, 8))':\n", a)
print()

b = zeros((4, 2), dtype=int)

print(b)
