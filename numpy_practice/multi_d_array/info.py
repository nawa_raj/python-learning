from numpy import *

"""
in python ndarray is possible withe the help of numpy package / liberary,
which helps to construct ndarray in python and array operations and provide more 
technique and tools to work with arrays in python.

"""


# 2d array...
print()
print("--------------numpy 2D Arrray----------------")
print()
a = array([[10, 20, 30, 40], [50, 60, 70, 80]])

print("2D array a: ".title(), a)
print()


# 3D array...
print()
print("--------------numpy 3D Arrray----------------")
print()
b = array([
    [[10, 20, 30, 40], [12, 13, 14, 15]],
    [[50, 60, 70, 80], [100, 101, 103, 104]]
])

c = array([
    [[1, 2], [2, 3], [4, 5]],
    [[6, 7], [8, 9], [10, 11]],
    [[12, 13], [14, 15], [16, 17]]
])

print("3D array b: ".title(), b)
print()
print("3D array 'c' with different format: ", c)
print()


# accessing ndArray...
print()
print("-------- Accessing NDArray with for loop------------")
print()

print("accessing array 'a' with for loop")
print()
for r in a:
    for col in r:
        print(col, end=", ")


print()
print("accessing array 'c' with for loop")
print()
for x in c:
    for y in x:
        for z in y:
            print(z, end=", ")


# accessing ADArray with...
print()
print("-------- Accessing NDArray with while loop------------")
print()

print("accessing array 'a' with while loop")
print()

i = 0
while i < len(a):

    j = 0
    while j < len(a[i]):
        print(a[i][j], end=", ")
        print()
        j += 1

    i += 1

print()
print("accessing array 'c' with while loop")
print()

i = 0
while i < len(c):
    j = 0

    while j < len(c[i]):
        k = 0

        while k < len(c[i][j]):
            print(c[i][j][k], end=", ")
            print()
            k += 1

        j += 1
    i += 1
