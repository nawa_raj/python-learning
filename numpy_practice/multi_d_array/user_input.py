from numpy import *


# taking user required array and create array for them with for loop
print()
ur = int(input("Enter No. of Rows: "))
uc = int(input("Enter No. of Columns: "))


a = zeros((ur, uc), dtype=int)

print()
print(f"orginal zeros({ur}, {uc}): ", a)
print()


for r in range(len(a)):

    for c in range(len(a[r])):
        x = int(input("Enter Element: "))
        a[r][c] = x

print()
print("user array: ", a)
print()


# taking user inputs using while loop
print()
ur = int(input("Enter No. of Rows: "))
uc = int(input("Enter No. of Columns: "))


b = zeros((ur, uc), dtype=int)

print()
print(f"orginal zeros({ur}, {uc}): ", b)
print()

i = 0
while i < len(b):
    j = 0

    while j < len(b[i]):
        x = int(input("Enter Element: "))
        a[i][j] = x

        j += 1
    i += 1

print()
print("user array: ", a)
