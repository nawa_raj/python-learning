from numpy import *


# faltten() function...

"""
flatten method is used to convert 2D, 3D array to 1D array.

"""

#  2D array
a = array([
    [[10, 20, 30, 40], [12, 13, 14, 15]],
    [[50, 60, 70, 80], [100, 101, 103, 104]]
])

# 3D array
b = array([
    [[1, 2], [2, 3], [4, 5]],
    [[6, 7], [8, 9], [10, 11]],
    [[12, 13], [14, 15], [16, 17]]
])


print()
print("---------- flatten method -----------")
print()

print("2D array 'a': ", a)
print()
print("3D array 'b': ", b)
print()


print("convert 2D into 1D with 'a.flatten()': ", a.flatten())
print("convert 3D into 1D with 'b.flatten()': ", b.flatten())
print()
