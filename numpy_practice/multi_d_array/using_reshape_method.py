from numpy import *


# reshape() function
"""
This function is used to change the shape of array. We can convert 1D to 2D or 3D array and vice versa. 
The new array should have same numbers of elements as in the original array

2D syntax:
    numpy.reshape(array, (rows, columns))

3D syntax:
    numpy.ones(array, (no.of array, rows, column))

"""


# create ones array with 5 rows and 8 columns
a = ones((5, 8), dtype=int)

print()
print("------- reshape functions ----------")

print()
print("orginal array: ", a)
print()

print("Convert 2D array into 1D 'reshape(a, (40))':\n", reshape(a, (40)))
print()

print("Convert 2D array into 3D 'reshape(a, (2, 4, 5))':\n", reshape(a, (2, 4, 5)))
print()
