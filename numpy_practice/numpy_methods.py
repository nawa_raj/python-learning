from numpy import *


# nonzero() function...
"""
nonzero() function return array of indexes of non zero element of array.

this function is used to determine the position of elements which are non zero. 
This function returns an array that contains the indexes of the element which are not equal to zero.

syntax: 
    numpy.nonzero(array)
 
"""

arr1 = arange(1, 10)
arr2 = full(5, 0)

arr1[2] = arr2[3]
arr1[7] = arr2[4]
arr1[5] = arr2[0]

arr3 = arr1
arr1 = arange(1, 10)
arr4 = append(arr1, arr2)

print()
print("initial arrays: ")
print("array arr1: ", arr1)
print("array arr2: ", arr2)
print("array arr2: ", arr3)


print()
print("-------- numpy nonzeor() function ---------")

print()
print("nonzero(arr1): ", nonzero(arr1))
print("nonzero(arr2): ", nonzero(arr2))
print("nonzero(arr3): ", nonzero(arr3))
print()


# view() function...
"""
view() function is used to construct a new view of array with a same data of the existing array.
the existing array and new array will share different memory location. 

if the new array get modified, the existing will also be modified as the elements in both the arrays 
will be like mirror image.

syntax: 
    arr2 = arr1.view()
 
"""

print()
print("------------- View() Method -----------------")
print()

arr1view = arr1.view()
print("arr1view  array as view of arr1 \n 'arr1vie = arr1.view()': ", arr1view)

arr1view[2] = 100

print("value modified of arr1view as 'arr1view[2] = 100': ", arr1view)
print("orginal arr1: ", arr1)
print()


# copy() function...
"""
copy() function is used to create a copy of existing array. If the new get modified, the existing array
will not be affected and vice-versa. both arrays are independent.

syntax:
    arr2 = arr1.view()
 
"""

print()
print("------------- copy() Method -----------------")
print()

arr2copy = arr2.copy()
print("arr1copy  array as copy of arr1 'arr2copy = arr1.copy()': ", arr2copy)
print("location of array arr1: ", id(arr1))
print("location of array arr1view: ", id(arr2copy))

arr2copy[3] = 300
print()
print("value modified of arr2copy as 'arr2copy[3] = 300': ", arr2copy)
print("orginal arr2: ", arr2)
print()


# python's any() and all() built in functions
"""

any() function returns True, if any one element of the iterable is True. If iterable is empty it return false.
all() function return True, if all elements of the iterable are True or iterable is empty. 


its usecase is defined in comparing_array.py file.

"""


# other numpy methods...
a = array([
    [10, 11, 12, 13],
    [20, 21, 22, 23],
    [30, 31, 32, 33],
    [40, 41, 42, 43]
], dtype=int)

b = array([10, 20, 30, 40, 50, 60])

print()
print("array a: ", a)
print("array b: ", b)
print()


# numpy sum attribute of array
print("1D array b.sum(): ", b.sum())
print("2D array a.sum(): ", a.sum())
print()


# numpy take attribute of array
print("1D array b.take(2): ", b.take(2))
print("2D array a.take(2, 1): ", a.take(2, 1))
print()
