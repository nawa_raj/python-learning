from numpy import *


# useful attributes of numpy arrays are:

a = array([
    [10, 11, 12, 13],
    [20, 21, 22, 23],
    [30, 31, 32, 33],
    [40, 41, 42, 43]
], dtype=int)

b = array([10, 20, 30, 40, 50, 60])

print()
print("array a: ", a)
print("array b: ", b)


# numpy ndim attribute of array
print()
print("1D array b.ndim: ", b.ndim)
print("2D array a.ndim: ", a.ndim)
print()


# numpy shape attribute of array
print("1D array b.shape: ", b.shape)
print("2D array a.shape: ", a.shape)
print()


# numpy size attribute of array
print("1D array b.size: ", b.size)
print("2D array a.size: ", a.size)
print()


# numpy itemsize attribute of array
# it find memory size of array element in bytes
print("1D array b.itemsize: ", b.itemsize)
print("2D array a.itemsize: ", a.itemsize)
print()


# numpy dtype attribute of array
print("1D array b.dtype: ", b.dtype)
print("2D array a.dtype: ", a.dtype)
print()


# numpy nbytes attribute of array
# it find total number of bytes occupied by array
print("1D array b.nbytes: ", b.nbytes)
print("2D array a.nbytes: ", a.nbytes)
print()
