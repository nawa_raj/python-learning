from numpy import *


print()
print("-------- mathmetical operations with numpy arrays ---------------")


arry = array([100, 101, 102, 103, 104, 105, 106, 107, 108, 109])
test = full(10, 5, dtype=int)


# array addition...
print()
print("orginal array of 'arry': ", arry)
print("orginal array of 'test': ", test)

print()
print("------- addition of array -------------")
print()

print("add 5 to each emement of array: ", arry + 5)

print()
print("add 'test' with 'arry': ", arry + test)


# array subtraction...
print()
print("orginal array of 'arry': ", arry)
print("orginal array of 'test': ", test)
print()
print("------- subtraction  array -------------")
print()


print("subtract 5 from each emement of array: ", arry - 5)

print()
print("subtract 'test' from 'arry': ", arry - test)


# array multiplication...
print()
print("------- multiplication of array -------------")
print()
print("orginal array of 'arry': ", arry)
print("orginal array of 'test': ", test)
print()

print()
print("multiply 5 with each emement of array: ", arry * 5)

print()
print("multiply 'test' with 'arry': ", arry * test)
