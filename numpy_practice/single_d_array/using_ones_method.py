from numpy import *


# creating array using zeros() functions of numpy package
"""
ones() function is used to create an array with all one.

syntax:
arange(shape, dtype = float, order="C")

"""

print()
new_arry = ones(10, dtype=int)

print("create array from ones(10): ", new_arry)


# accessing new_arry data with for loop
print()
print("------ accessing new_arry data with for loop -------------")

for el in new_arry:
    print(el)


# accessing new_arry data with for loop
print()
print("------ accessing new_arry data with while loop -------------")

i = 0
while i < len(new_arry):
    print('index ', i,  " = ", new_arry[i])
    i += 1
