from numpy import *


# creating array using linspace() functions of numpy package
"""
syntax:

linspace(start, stop, num = 50, endpoint = True, retstep = False, dtype = None, axix = 0)
"""

print()
new_arry = linspace(1, 100, 10)

print("create array from linspace(1, 100, 10): ", new_arry)


# accessing new_arry data with for loop
print()
print("------ accessing new_arry data with for loop -------------")

for el in new_arry:
    print(el)


# accessing new_arry data with for loop
print()
print("------ accessing new_arry data with while loop -------------")

i = 0
while i < len(new_arry):
    print('index ', i,  " = ", new_arry[i])
    i += 1
