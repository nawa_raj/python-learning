import numpy as np


stu_roll = np.array([101, 102, 103, 104, 105])

print()
print(stu_roll)
print("array type: ", type(stu_roll))
print("array data type: ", stu_roll.dtype)


# change value of array element
print()
stu_roll[2] = 1000
print(
    "element value change 'stu_roll[2] =  1000' \n new array : ", stu_roll)


# array with for loop
print()
print("------- array with for loop ---------")

for el in stu_roll:
    print(el)


# accessing each element from array with range, len, and for loop
print()
print("------- array with range, len, and for loop ---------")

for index in range(len(stu_roll)):
    print("index ", index, " = ", stu_roll[index])


# accessing each element from array with range, len, and for loop
print()
print("------- array with len, and while loop ---------")

i = 0
while i < len(stu_roll):
    print("index ", i, " = ", stu_roll[i])
    i += 1


# calculate sum from array
print()
print("sum of array 'stu_roll.sum()': ", stu_roll.sum())
