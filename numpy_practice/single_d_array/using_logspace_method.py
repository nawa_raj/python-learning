from numpy import *


# creating array using logspace() functions of numpy package
"""
logspace() function is used to create an array with evenly spaced numbers logarithmically. the 
sequence starts at base ** start(base to the power of start) and ends with base.

syntax:
linspace(start, stop, num = 50, endpoint = True, base = 10.0, dtype = None, axix = 0)

"""

print()
new_arry = logspace(1, 10, 10)

print("create array from logspace(1, 100, 10): ", new_arry)


# accessing new_arry data with for loop
print()
print("------ accessing new_arry data with for loop -------------")

for el in new_arry:
    print(el)


# accessing new_arry data with for loop
print()
print("------ accessing new_arry data with while loop -------------")

i = 0
while i < len(new_arry):
    print('index ', i,  " = ", new_arry[i])
    i += 1
