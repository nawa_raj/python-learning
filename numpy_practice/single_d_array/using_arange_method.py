from numpy import *


# creating array using arange() functions of numpy package
"""
arange() function is used to create an array with group of elements from 
start to one element prior to stop in steps of stepsize.

syntax:
arange(start, stop, stepsize, dtype = None)

"""

print()
new_arry = arange(0, 100, 10)

print("create array from arange(0, 100, 10): ", new_arry)


# accessing new_arry data with for loop
print()
print("------ accessing new_arry data with for loop -------------")

print()
test = [x for x in new_arry if x != 0]
print("array without zero: ", test)

for el in new_arry:
    print(el)


# accessing new_arry data with for loop
print()
print("------ accessing new_arry data with while loop -------------")

i = 0
while i < len(new_arry):
    print('index ', i,  " = ", new_arry[i])
    i += 1
