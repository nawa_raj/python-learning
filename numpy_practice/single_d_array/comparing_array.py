from numpy import *


arr1 = arange(1, 10)
arr2 = logspace(1, 2, 9, endpoint=True)
arr3 = arange(1, 10)

print()
print("arr1: ", arr1, "\t\t", "Length of array: ", len(arr1))
print()
print("arr2: ", arr2, "\t\t", "Length of array: ", len(arr2))
print()
print("arr3: ", arr3, "\t\t", "Length of array: ", len(arr3))
print()

arr3[2] = 500
arr3[7] = 100

# checking is equal values...
print("compare arr1 == arr2", arr1 == arr2)
print("compare arr1 == arr3", arr1 == arr3)
print()

# checking is less values...
print("compare arr1 < arr2", arr1 < arr2)
print("compare arr2 < arr3", arr2 < arr3)
print()


# checking is less values...
print("compare arr2 <= arr1", arr2 <= arr1)
print("compare arr1 >= arr3", arr1 >= arr3)
print("compare arr1 <= arr3", arr1 <= arr3)
print()


# python's any() and all() built in functions
"""

any() function returns True, if any one element of the iterable is True. If iterable is empty it return false.
all() function return True, if all elements of the iterable are True or iterable is expty. 

"""


a1 = array([])
a2 = array([])

print("modified array arr3: ", arr3)

print("array a1: ", a1)
print("array a2: ", a2)
print()
print("python any() function: ", any(a1))
print("python all() function: ", all(a2))


print()
print("all(arr1 <= arr3): ", all(arr1 <= arr3))
print("any(arr1 <= arr3): ", any(arr1 <= arr3))

print()
print("all(arr1 >= arr3): ", all(arr1 >= arr3))
print("any(arr1 < arr3): ", any(arr1 < arr3))
