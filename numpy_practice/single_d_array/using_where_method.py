from numpy import *


"""

where() function is return an new array which contains, return element chosen from expression1 or 
expression2 depending on conditions. if condition is True expression1 is executed else expression2.

it return new array from both array's elements where condition is satisfied.

syntax: 
numpy.where(condition, expression1, expression2)

"""


a = array([100, 200, 300, 400, 500])
b = array([100, 20, 300, 40, 500])
c = array([10, 200, 30, 400, 50])

print()
print("----new array with where functions-----")
print("array a: ", a)
print("array b: ", b)
print("array c: ", c)
print()

print("a > b", a > b)
print("a < b", a < b)
print()


print("where(a > b, a, b): ", where(a > b, a, b))
print("where(a < b, a, b): ", where(a < b, a, b))
print("where(a <= b, a, b): ", where(a <= b, a, b))
print("where(b < c, b, c): ", where(b < c, b, c))
print()


print("where(a < 10, a, 10*a): ", where(a < 500, a, 10*a))
print()
