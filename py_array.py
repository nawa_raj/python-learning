# import array
from array import *


"""
 we can import array by two technique 
    firstly import array 
    

    if we import array like follows:
        from array import* 
        
        then we can do like this:

        array("type", [data])

    if we import array like follows:
        import array 

        then we can do like this:

        array.array("type", [data])

 """


"""
array are mainly two types in python and we can use them to get job done
"""


print()
print("----- array with 'from array import*' method -----")
stu_roll = array("i", [101, 102, 103, 104, 105])
print()
print(stu_roll)

# array with for loop
print()
print("----- array with for loop -----")

for stu in stu_roll:
    print(stu)


# array with for loop and range function
print()
print("----- array with for  loop and range function -----")

for stu_index in range(len(stu_roll)):
    print(stu_roll[stu_index])


# array with while loop
print()
print("----- array with while loop -----")

i = 0
while i < len(stu_roll):
    print(stu_roll[i])
    i += 1


"""
Built in array methods are as follows:
"""

print()
print("----- Buildin Array Methods -----")


# append() method
print()
print("--- old stu_roll array: ", stu_roll)

stu_roll.append(106)
print("stu_roll.append(106): ", stu_roll)


# insert() method
print()
print("--- old stu_roll array: ", stu_roll)

stu_roll.insert(3, 109)
print("stu_roll.insert(3, 109): ", stu_roll)


# pop() method
print()
print("--- old stu_roll array: ", stu_roll)

popped_value = stu_roll.pop(2)
print("stu_roll.pop(2): ", stu_roll)
print("poped value: ", popped_value)


# remove() method
print()
print("--- old stu_roll array: ", stu_roll)

removed_value = stu_roll.remove(102)
print("stu_roll.remove(102): ", stu_roll)


# index() method
print()
print("--- old stu_roll array: ", stu_roll)

print("stu_roll.index(109): ", stu_roll.index(109))


# reverse() method
print()
print("--- old stu_roll array: ", stu_roll)

stu_roll.reverse()
print("stu_roll.reverse(): ", stu_roll)


# extend() method
print()
print("--- old stu_roll array: ", stu_roll)

narry = array("i", [108, 110, 102, 103, 106, 108, 101, 102, 104, 106, 109])
print("new defined array 'narry': ", narry)

stu_roll.extend(narry)
print("after extend 'stu_roll.extend(narry)': ", stu_roll)


# count() method
print()
print("--- old stu_roll array: ", stu_roll)

print("stu_roll.count(102): ", stu_roll.count(102))


# change value form accesing element
print()
stu_roll[2] = 1000
print("change value 'stu_roll[2] = 1000' now new array: ", stu_roll)


# slicing of array
print()
print("------------ slicing of array ---------------------")
print()
print("--- old stu_roll array: ", stu_roll)

print()
print("stu_roll[1:5]: ", stu_roll[1:5])

print()
print("stu_roll[3:]: ", stu_roll[3:])

print()
print("stu_roll[:5]: ", stu_roll[:5])

print()
print("stu_roll[:-5]: ", stu_roll[:-5])

print()
print("stu_roll[-5:]: ", stu_roll[-5:])

print()
print("stu_roll[-5:-3]: ", stu_roll[-5:-3])

print()
print("stu_roll[0:7:2]: ", stu_roll[0:7:2])

print()
print("stu_roll[0::2]: ", stu_roll[0::2])
