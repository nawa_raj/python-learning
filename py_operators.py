# to run this file run command: python \.arithmetic_operators.py

"""
operators are symboles which are used to perfom some operations.
"""

# value assignment...
a = 10
b = 5

# --------------- Arithmetic Operators --------------------------
print("\n")
print("---------- Arithmetic Operators -----------")

"""
Arthemetic operators are used to perform basic arithmetic operations 
like addition, subtraction, division etc.
"""

print(f"value of :\n a = {a} \n b = {b}")
print("Addition (a + b): \t \t", a + b)
print("Subtraction (a - b): \t \t", a - b)
print("Multiplication (a * b): \t \t", a * b)
print("Division (a / b): \t \t", a / b)

# it gives reminder only
print("Modulus (a % b): \t \t", a % b)

# it gives power of the base number
print("Exponent (a ** 4): \t \t", a ** 4)

# it gives only exact divisable value
print("Floor Division (a // b): \t \t", a // b)


# --------------- Relational / Comparison Operators --------------------------
print("\n")
print("---------- Relational / Comparison Operators -----------")

"""
Relational or Comparison operators are used for comparing two values 
and its produce logical value i.e. True or False
"""

print(f"value of :\n a = {a} \n b = {b}")
print("Less Tthan (a < b): \t\t", a < b)
print("Greater Than (a > b): \t\t", a > b)
print("Less Than or Equals to (a <= b): \t\t", a <= b)
print("Greater Than or Equals to (a >= b): \t\t", a >= b)
print("Equals to (a == b): \t\t", a == b)
print("Not Equals to (a != b): \t\t", a != b)


# ----------------- logical operators --------------------------
print("\n")
print("---------- Logical Operators -----------")

"""
Logical operators are used to connect more relational operation to form a complex expression.
A value obtained by a logical expression is always logical i.e. True or False
"""

print(f"value of :\n a = {a} \n b = {b}")
print("Logical and (a > 2) and (b <= 5): \t\t", (a > 2) and (b < 5))
print("Logical or (a < 2) or (b <= 5): \t\t", (a < 2) or (b <= 5))
print("Logical not  'not(a < 2)': \t\t", not(a < 2))


# ----------------- Assignment operators --------------------------
print("\n")
print("---------- Assignment Operators -----------")

"""
Assignment operators are used to perform arithmetic operations while 
assigning a value to a variable
"""

print(f"value of :\n a = {a} \n b = {b}")

# calculations...
c = a+b
print("Only value Assign (c = a + b): \t\t", c)

k = 0
print("k = \t", k)
k += a
print("Addition while Value Assign (k += a): \t\t", k)

k = 100
print("k = \t", k)
k -= a
print("Subtraction while Value Assign (k -= a): \t\t", k)

k = 10
print("k = \t", k)
k *= a
print("Multiplication while Value Assign(k *= a): \t\t", k)


k = 50
print("k = \t", k)
k /= a
print("Division while Value Assign(k /= a): \t\t", k)


k = 10
print("k = \t", k)
k //= a
print("Floor Division while Value Assign(k //= a): \t\t", k)

k = 21
print("k = \t", k)
k %= a
print("Modulo while Value Assign(k %= a): \t\t", k)


k = 2
print("k = \t", k)
k **= a
print("Getting Power of k while Value Assign(k **= a): \t\t", k)


# ----------------- Bitwise Operators --------------------------
print("\n")
print("---------- Bitwise Operators -----------")

"""
Bitwise operators are used to perform operatuions at binary digit level.
these operators are used only in special applications where optimized use
of storage is required
"""

print(f"value of :\n a = {a} \n b = {b}")


print("Bitwise NOT (~a): \t\t", ~a)
print("Bitwise & (a & b): \t\t", a & b)
print("Bitwise OR (a | b): \t\t", a | b)
print("Bitwise XOR (a ^ b): \t\t", a ^ b)
print("Bitwise Left Shift (a << 2): \t\t", a << 2)
print("Bitwise Right Shift (b >> 2): \t\t", a >> 2)


# ----------------- Membership Operators --------------------------
print("\n")
print("---------- Membership Operators -----------")

"""
The membership operators are useful to test for membership in a sequence
such as string, list, tuple, and dictionaries
"""


tx = "welcome to nepal"
tx1 = "welcome to my home"

print(f"value of :\n tx = {tx} \n tx1 = {tx1}")


print("Membership 'in' ('to' in tx): \t\t", "to" in tx)
print("Membership 'not in' ('to' not in tx1): \t\t", "nepal" not in tx1)


# ----------------- Identity Operators --------------------------
print("\n")
print("---------- Identity Operators -----------")

"""
Identity operator is used to compare whatever two objects are same or not.
It return True if memory location of two objects are same else it Return False
"""


print(f"value of :\n a = {a} \n b = {b}")
print("Identity is (a is b): \t\t", a is b)
print("Identity is (a is not b): \t\t", a)


# ----------------- Operators Precedence --------------------------
print("\n")
print("---------- Operators Precedence -----------")

"""
This indicate that which operation should be perform first from the long pexression
"""


print(f"value of :\n a = {a} \n b = {b}")
print("Python Arithmetic calculations also implies BODMAS rules: '(1 + 1)*2**4 // 3 + 4 - 1' : \t\t", (1+1)*2**4//3+4-1)
