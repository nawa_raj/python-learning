# global keyword in python
# z = 60  # it is declared as global value
def add():
    x, y = 20, 30

    global z
    z = 50  # it is also declared as global variable

    return x + y + z


print("\n------------ global key ---------\n")
print("sum: ", add())
print("we can access 'z' value outsie of function: ", z)
print()


# we define global variable in simple form
z = 60


def add():
    x, y = 20, 30

    # it throw error because z is treated as local variable but it is not defined here as local variable
    # z = z + x + y

    a = z + x + y  # its fine because 'a' has not a same variable name here

    return a


print("\n------------ global variable ---------\n")
print("sum: ", add())
print()


# accessing globla variable inside function where local with same name is present:
a = 67


def globalTest():
    a = 14
    x = globals()["a"]

    print("local variable 'a': ", a)
    print("Global variable 'a': ", x)


print(
    "\n---------- accessing global variable inside function where same variable present -------------\n"
)
globalTest()
print()


# variable scope:
def variable_block_scope():
    v = 20

    if v > 10:
        l = 10
        print("if block local variable 'l': ", l)

    # we can accesss if block local variable form outside
    print("trying to access if block local variable 'l': ", l)


print("\n----------- variable block scope ----------------\n")
variable_block_scope()
print()


# variable function scope:
def variable_function_scope():
    v = 20

    def inner():
        l = 10
        print("inner function local variable 'l': ", l)
        print("trying to accesss parent function variable 'v': ", v)

    inner()

    # we cannot accesss inner function local variable form outside
    # print("trying to access inner function local variable 'l': ", l)


print("\n----------- variable function scope ----------------\n")
variable_function_scope()
print()
